# README #

This repository contains the files necessary to compile programs that use the MD5 hash function.

On Ubuntu systems, you will need to link to the `crypto` library when compiling. You can use either
`gcc` or `clang` to compile.

    gcc myprog.c md5.c -o myprog -l crypto
	
